#include <stdio.h>

// Print Column according with index

int main()
{
    int row, col;
    scanf("%d %d", &row, &col);
    int ar[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            scanf("%d", &ar[i][j]);
        }
    }

    // for (int i = 0; i < row; i++)
    // {
    //     for (int j = 0; j < col; j++)
    //     {
    //         printf("%d ",ar[i][j]);
    //     }
    //     printf("\n");
    // }

    int n;
    scanf("%d", &n);
    for (int i = 0; i < row; i++)
    {
        printf("%d ", ar[i][n]);
    }
    return 0;
}